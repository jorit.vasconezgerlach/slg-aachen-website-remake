<?php

    // Define HTML path
    $pageHtmlPath = "page.html";
    
    // Check if HTML doc works and prepair output
    if (file_exists($pageHtmlPath)) {
        $output = file_get_contents($pageHtmlPath);
    } else {
        $output = "Interner Fehler! Verusche es bitte später erneut. Fehlercode: 404"
    }

    echo $output;

?>
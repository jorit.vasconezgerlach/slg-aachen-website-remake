<?php

    // Get visitor data from form
    $visitor_name = $_POST['name'];
    $visitor_email = $_POST['email'];
    $visitor_tel = $_POST['tel'];
    $visitor_message = $_POST['message'];

    // Get the HTML template
    $mail_template = "contact-email-template.html";

    // Test if HTML template has loaded
    if(file_exists($mail_template))
        $send_message = file_get_contents($mail_template);
    else
        $send_message = "Data from the submission:\n".
                        "Name: $visitor_name\n".
                        "Email: $visitor_email\n".
                        "Message: $visitor_message\n";

    // Define the variables to swap
    $swap_variables = array(
        "{VISITOR_NAME}" => $visitor_name,
        "{VISITOR_MAIL}" => $visitor_email,
        "{VISITOR_TEL}" => $visitor_tel,
        "{VISITOR_MESSAGE}" => $visitor_message
    );

    // Replace in the actual message
    foreach(array_keys($swap_variables) as $key){
        $send_message = str_replace($key, $swap_variables[$key], $send_message);
    }

    // Define the data to send
    $sended_subject = "Kontakt Nachricht";
    $send_to = 'jorit@vasconezgerlach.de';
    
    // Prepair the email headers
    $headers = "From: SLG Aachen Kontakt <kontakt@slg-aachen.de>\r\n";
    $headers .= "Reply-To: $visitor_email\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; chartset=UTF-8\r\n";

    // Start sending email
    if(mail($send_to,$sended_subject,$send_message,$headers))
        echo header("Location: kontakt.html");
    else
        echo 'try again';

?>
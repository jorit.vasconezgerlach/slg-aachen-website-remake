<?php

    // Define HTML path
    $pageHtmlPath = "page.html";
    
    // Check if HTML doc works and prepair output
    if (file_exists($pageHtmlPath)) {
        $pageHtml = file_get_contents($pageHtmlPath);
    } else {
        // Open error HTML
        $output = file_get_contents("http://localhost/slg-aachen-website-remake/assets/docs/error-pages/404/page.html");
        die($output);
    }

    echo $pageHtml;

?>
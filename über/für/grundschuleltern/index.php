<?php

    // Define HTML path
    $pageHtmlPath = "page.html";
    
    // Check if HTML doc works and prepair output
        if (file_exists($pageHtmlPath)) {
            $pageHtml = file_get_contents($pageHtmlPath);
        } else {
            // Open error HTML
            $output = file_get_contents("http://localhost/slg-aachen-website-remake/assets/docs/error-pages/404/page.html");
            die($output);
        }

    // Prepair JSON
        // Path to the json
        $fürGrundschulelternJsonPath = "http://localhost/slg-aachen-website-remake/assets/api/über/für/grundschuleltern/videos/get.json";
        // Get the content from the url
        $fürGrundschulelternJson = file_get_contents($fürGrundschulelternJsonPath);
        // Decode the JSON
        $fürGrundschulelternArray = json_decode($fürGrundschulelternJson,true);

    // Check for primary video
        //
        if(isset($_GET['file'])) {
            $primaryVidFile = $_GET['file'];
        } else {
            $primaryVidFile = $fürGrundschulelternArray['videos'][0]['fileName'];
        }
    
        echo $primaryVidFile;

    // Get highest fileName

        // dev-to-do: This function should display the highest file and if there is no highest file pass an bool with false

        // function highestFileName(array $arrayP) {
        //     print_r($arrayP);
        //     $arrayP = array();
        //     if(isset($arrayP['videos'][0]['fileName'])) {
        //         echo 'hex';
        //     } else {
        //         echo $arrayP['videos'][0]['fileName'];
        //     }
        // }

        // echo highestFileName($fürGrundschulelternArray);
         
    // Load the other videos
        function loadOtherVideos(array $arrayP) {

            $output = "";

            foreach($arrayP['videos'] as $key) {

                $thumbnailPath = "http://localhost/slg-aachen-website-remake/assets/content/über/für/grundschuleltern/thumbnails/SLG Aachen - ".$key['fileName']." (Thumbnail).".$key['thumbnailFormat'];
                $vidPath = "http://localhost/slg-aachen-website-remake/assets/content/über/für/grundschuleltern/videos/SLG Aachen - Ankommen.".$key['vidFormat'];

                
                $output .= '<div class="video" onclick="test()">';
                    $output .= '<img src="'.$thumbnailPath.'">';
                    $output .= '<div class="text">';  
                        $output .= '<h3>'.$key['title'].'</h3>';
                        $output .= '<p>'.$key['description'].'</p>';
                    $output .= '</div>';
                $output .= '</div>';
                // $output .= $vidPath;
            };
        
            return $output;
        }

    // Define the variables to swap
        $swap_variables = array(
            "{VIDEOS}" => loadOtherVideos($fürGrundschulelternArray),
        );

    // Replace in the actual message
        foreach(array_keys($swap_variables) as $key){
            $output = str_replace($key, $swap_variables[$key], $pageHtml);
            echo $output;
        }

?>